<?php
require('pdo.php');

$Tabela = $_POST['tabela'];

if ($Tabela=='carro') {

	$Nome = $_POST['namecarro'];
	$Valor = $_POST['valorcarro'];

	$insert = $db->prepare("INSERT INTO carro (nome_carro, valor_carro) VALUES(:nomeCarro,:valorCarro)");
	$insert->execute([
		'nomeCarro' => $Nome, 'valorCarro' => $Valor
	]);
	echo "Veiculo Inserido!";
}elseif($Tabela=='acessorio'){

	$Nome = $_POST['nameacessorio'];
	$Valor = $_POST['valoracessorio'];

	$insert = $db->prepare("INSERT INTO acessorio (nome_acessorio, valor_acessorio) VALUES(:nomeAcessorio,:valorAcessorio)");
	$insert->execute([
		'nomeAcessorio' => $Nome, 'valorAcessorio' => $Valor
	]);
	echo "Acessorio Inserido!";
}elseif($Tabela=='agendamento'){

	$Nome = $_POST['nameagendamento'];
	$Valor = $_POST['idcarro'];

	$insert = $db->prepare("INSERT INTO agendamento (nome_agendamento, fk_carro) VALUES(:nomeAgendamento,(SELECT id_carro FROM carro WHERE nome_carro=:fkAgendamento))");
	$insert->execute([
		'nomeAgendamento' => $Nome, 'fkAgendamento' => $Valor
	]);
	echo "Agendamento Inserido!";
}elseif($Tabela=='carroUp'){
	$idcarro=$_POST['idCarro'];
	$Nome = $_POST['nomecarro'];
	$Valor = $_POST['valorcarro'];
	
	$update = $db->prepare("UPDATE carro SET nome_carro=:nomeCarro,valor_carro=:valorCarro WHERE id_carro = :idCarro");
	$update->execute([
		'nomeCarro' => $Nome, 'valorCarro' => $Valor, 'idCarro' =>$idcarro
	]);
	echo "Feito Update!";
}elseif($Tabela=='acessorioUp'){
	$idacessorio=$_POST['idacessorio'];
	$Nome = $_POST['nomeacessorio'];
	$Valor = $_POST['valoracessorio'];
	
	$update = $db->prepare("UPDATE acessorio SET nome_acessorio=:nomeAcessorio,valor_acessorio=:valorAcessorio WHERE id_acessorio = :idAcessorio");
	$update->execute([
		'nomeAcessorio' => $Nome, 'valorAcessorio' => $Valor, 'idAcessorio' =>$idacessorio
	]);
	echo "Feito Update!";
}elseif($Tabela=='carroRmv'){

	$idcarro=$_POST['idCarro'];

	$removeAg = $db->prepare("DELETE FROM agendamento WHERE fk_carro = :idCarro");
	$removeAg->execute([
		'idCarro' => $idcarro
	]);

	$remove = $db->prepare("DELETE FROM carro WHERE id_carro = :idCarro");
	$remove->execute([
		'idCarro' => $idcarro
	]);
	echo "Feito Remocao!";
}
elseif($Tabela=='acessorioRmv'){
	$idacessorio=$_POST['idAcessorio'];
	
	$update = $db->prepare("DELETE FROM acessorio WHERE id_acessorio = :idAcessorio");
	$update->execute([
		'idAcessorio' =>$idacessorio
	]);
	echo "Feito Remocao!";
}
