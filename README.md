# Teste Connectmix

## instalação
1. Instalar WAMP 3.1.9 ou semelhantes
2. Verificar se a instação possui mysql integrado
3. Executar o sql para a criação das tabelas.
4. Após a criacao do banco verificar arquivo pdo.php

## sql

CREATE TABLE IF NOT EXISTS carro(
    id_carro INT AUTO_INCREMENT,
    nome_carro VARCHAR(255) NOT NULL,
    valor_carro VARCHAR(255) NOT NULL,
    PRIMARY KEY (id_carro)
);

CREATE TABLE IF NOT EXISTS acessorio( 
    id_acessorio INT AUTO_INCREMENT,
    nome_acessorio VARCHAR(255) NOT NULL,
    valor_acessorio VARCHAR(255) NOT NULL,
    PRIMARY KEY (id_acessorio)
);

CREATE TABLE IF NOT EXISTS agendamento(
    id_agendamento INT PRIMARY KEY AUTO_INCREMENT,
    nome_agendamento VARCHAR(255) NOT NULL,
    fk_carro int, 
    CONSTRAINT fk_AgenCarro FOREIGN KEY (fk_carro) REFERENCES carro (id_carro)
);
